Analyse Avancée sur la section de 'communication' :
    S3 :
        Pour cette section, le travail est minime, effectivement le seul
        travail à faire est juste d'ouvrir la connection et d'attendre la
        carte.
    S4 :
        Concevoir une fonction qui envoie les 'poses' :
            Celle-ci prendra en argumennts des coordonnées et enverra au
            serveur un message du type 'A:xy'.
            Cette fonction attendra le retour du serveur soit 'VALI' soit
            'INVA'.
            Elle attendra alors le coup du serveur sous la forme 'B:xy' ou
            un message 'FINI' si ce dernier ne peut pas jouer.
            Si le serveur n'a pas envoyé fini :
                Elle attendra aussi le message suivant du serveur qui nous
                indique si la partie s'achève ou non.
            Enfin elle nous retournera un objet contenant :
                Une Chaîne de Caractère indiquant si le coup joué était
                valide ou non.
                Des coordonnées indiquant le coup de l'I.A. (Si cette
                dernière n'a pas pu jouer celle-ci vaudra -1,-1)
                Une Chaine de Caractère indiquant si la partie se poursuit