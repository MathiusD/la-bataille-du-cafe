﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesBatailleCafe
{
    public class Case
    {
        private char groupe;
        private string type;
        private bool frontiereN;
        private bool frontiereS;
        private bool frontiereE;
        private bool frontiereO;
        private Graine graine;
        private List<Case> casesFrontiere;

        public string Type { get => type; set => type = value; }
        public char Groupe { get => groupe; set => groupe = value; }
        public bool FrontiereN { get => frontiereN; }
        public bool FrontiereS { get => frontiereS; }
        public bool FrontiereE { get => frontiereE; }
        public bool FrontiereO { get => frontiereO; }
        public Graine Graine { get => graine; set => graine = value; }
        public List<Case> CasesFrontiere { get => casesFrontiere; set => casesFrontiere = value; }

        
        public Case(char groupe, bool frontiereN, bool frontiereS, bool frontiereE, bool frontiereO, Graine graine, String type)
        {
            this.groupe = groupe;
            this.frontiereN = frontiereN;
            this.frontiereS = frontiereS;
            this.frontiereE = frontiereE;
            this.frontiereO = frontiereO;
            this.graine = graine;
            this.type = type;
            this.casesFrontiere = null;
        }
        public Case(char groupe, bool frontiereN, bool frontiereS, bool frontiereE, bool frontiereO) : this(groupe, frontiereN, frontiereS, frontiereE, frontiereO, null, null)
        {

        }
        public Case(Case c) : this(c.Groupe, c.FrontiereN, c.FrontiereS, c.FrontiereE, c.FrontiereO, c.Graine, c.Type)
        {

        }

        public Case(bool frontiereN, bool frontiereS, bool frontiereE, bool frontiereO) : this(' ' , frontiereN, frontiereS, frontiereE, frontiereO)
        {

        }

    }
}
