using System;
namespace ClassesBatailleCafe
{
    public class Utils
    {
        public static bool VerifPose(Plateau plateau, byte x, byte y)
        {
            //Nous r�cup�rons la case choisie par l'IA
            //Cela nous permettra de savoir si la case choisie est une case
            //o� il est impossible de poser ou non
            Case[,] CarteDeJeu = plateau.Carte;
            Case CaseChoisie = CarteDeJeu[x,y];
            //Les variables coordonnee et DerniereGraine nous serviront respectivement:
            //      -� v�rifier que les coordonn�es sont diff�rentes en X ET en Y par rapport � la Derni�re Graine Pos�e
            //      -� �viter une redondance et avoir des lignes trop longues pour utiliser la derni�re graine pos�e
            short[] coordonnee = { x,y };
            short[] DerniereGraine = plateau.DerniereGrainePosee;
            //Ici, si la case choisie a un type �tant de type Mer ou Foret, on renvoit false (impossible de poser)
            if (CaseChoisie.Type.Equals("Mer") || CaseChoisie.Type.Equals("Foret"))
            {
                return false;
            }
            else
            {
                //Sinon, si la case a une graine sur elle, on renvoie false
                if (CaseChoisie.Graine != null)
                {
                    return false;
                }
                else
                {
                    //Si c'est le premier tour, cette v�rification est pass�e
                    if (plateau.DerniereGrainePosee != null)
                    {
                        //sinon, si les coordonn�es X ET Y sont diff�rents de celle de la derni�re graine
                        //nous renvoyons un false, car il faut qu'exactement une des deux coordon�es soit
                        //�gale � celle de la derni�re graine (X doit �tre = X ou Y doit �tre �gale � Y
                        //pour que la graine puisse �tre pos�e)
                        if (coordonnee[0] != DerniereGraine[0] && coordonnee[1] != DerniereGraine[1])
                        {
                            return false;
                        }
                        //sinon, nous instancions les coordonn�es de la derni�re graines � nouveau
                        //et nous v�rifions si le groupe de la derni�re graine et de la case que nous voullons
                        //sont �gaux. Alors on renvoie False
                        short coordXDerGraine = DerniereGraine[0];
                        short coordYDerGraine = DerniereGraine[1];
                        if (plateau.Carte[x, y].Groupe == plateau.Carte[coordXDerGraine, coordYDerGraine].Groupe)
                        {
                            return false;
                        }
                    }
                }
                
            }
            //sinon, si, apr�s toutes ces v�rifications, rien ne g�ne, on renvoit True (peut �tre pos�e)
            return true;
        }

        public static int[] CompteurPoints(Plateau plateau)
        {
            return Comptage_Points.Compteur_points_paquet(plateau);
        }
    }
}