﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesBatailleCafe
{
    public class Decodage_Trame
    {
        /**
         * Fonction qui créer un tableau d'entier à partir d'un string du bon format
         * =========================================================================
         * :paramètre d'entrée trame: String : suite de nombres séparés à l'aide de ';' et '|' identifiant un terrain de jeu précis
         * :return tab_parc: int[,] : tableau de 10*10 contenant les valeurs de chaque case et donc les informations pour l'attribution des frontières
         */
        public static int[,] DecoupageTrame(string trame)
        {
            // tab_parc servira à stocker dans un tableau les valeurs récupérées en découpant la trame
            int[,] tabParc = new int[10, 10];
            // ligne_parc contiendra 10 string stockant chacune 10 valeurs séparées par des ':' après la première découpe
            string[] ligneParc = new string[10];
            // col_parc contiendra les 10 valeurs de chaque ligne sous forme de string après la découpe d'une ligne de ligne_parc
            string[] colParc = new string[10];

            // première découpe de la trame pour en isoler les lignes à l'aide de la fonction Split
            ligneParc = trame.Split('|');
            for (int indiceLigne = 0; indiceLigne < 10; indiceLigne++)
            {
                // découpe de chaque ligne pour en isoler les valeurs
                colParc = ligneParc[indiceLigne].Split(':');
                for (int indiceColonne = 0; indiceColonne < 10; indiceColonne++)
                {
                    // stockage des valeurs isolées dans un tableau d'int
                    tabParc[indiceLigne, indiceColonne] = int.Parse(colParc[indiceColonne]);
                }
            }
            return tabParc;
        }

        /**
         * Fonction qui attribue à chaque case des frontière
         * =============================================================================
         * :paramètre d'entrée tab_trame: int[,] : Tableau d'int contenant les infos nécessaire à l'attribution des frontières d'une case
         * :return Terrain: Case[,] : tableau de 10*10 cases contenant les informations suivantes :
         * Groupe = ' '
         * Type défini a postériori
         * Booléens d'idenfications de frontière correctement initialisés
         */
        public static Case[,] DetermineFrontieresCases(int[,] tabTrame)
        {
            Case[,] terrain = new Case[10,10];
            // parcours du tableau Terrain à l'aide de deux boucles "for" imbriquées
            for (int indiceLigne = 0; indiceLigne < 10; indiceLigne++)
            {
                for (int indiceColonne = 0; indiceColonne < 10; indiceColonne++)
                {
                    // initialisation de variables qui serviront à détecter la présence de frontière
                    bool frontN = false, frontS = false, frontE = false, frontO = false;
                    int valeurTest = tabTrame[indiceLigne, indiceColonne];
                    // les cases contenant une valeur >= à 32 sont soit de type Foret, soit de Type Mer, il n'y a donc aucun intérêt à modifier leurs frontières
                    if (valeurTest >= 32)
                    {
                        valeurTest = 0;
                    }
                    // Detection de la présence d'une frontière Est
                    if (valeurTest / 8 >= 1)
                    {
                        valeurTest -= 8;
                        frontE = true;
                    }
                    // Detection de la présence d'une frontière Sud
                    if (valeurTest / 4 >= 1)
                    {
                        valeurTest -= 4;
                        frontS = true;
                    }
                    // Detection de la présence d'une frontière Ouest
                    if (valeurTest / 2 >= 1)
                    {
                        valeurTest -= 2;
                        frontO = true;
                    }
                    // Detection de la présence d'une frontière Nord
                    if (valeurTest == 1)
                    {
                        frontN = true;
                        valeurTest = 0;
                    }
                    // Création d'une instance de Case dans le Terrain en fonction des frontières détectées
                    terrain[indiceLigne, indiceColonne] = new Case(frontN, frontS, frontE, frontO);
                }
            }
            return terrain;
        }

        /**
         * Fonction qui attribue à chaque case le bon type
         * =============================================================================
         * :paramètre d'entrée Terrain: Case[,] : Terrain dont les frontières ont été attribués
         * :return Terrain: Case[,] : tableau de 10*10 cases contenant les informations suivantes :
         * Groupe = ' '
         * Type défini correctement. Contient "Terre", "Mer" ou "Foret"
         * Booléens d'idenfications de frontière correctement initialisés
         */
        public static Case[,] DetermineTypeCases(Case[,] terrain, int[,] tabTrame)
        {
            // parcours du tableau Terrain à l'aide de deux boucles "for" imbriquées
            for (int indiceLigne = 0; indiceLigne < 10; indiceLigne++)
            {
                for (int indiceColonne = 0; indiceColonne < 10; indiceColonne++)
                {   
                    // attribution d'un type à une case en fonction de sa valeur
                    if (tabTrame[indiceLigne, indiceColonne] < 32)
                    {
                        terrain[indiceLigne, indiceColonne].Type = "Terre";
                    }
                    if (tabTrame[indiceLigne, indiceColonne] >= 32 && tabTrame[indiceLigne, indiceColonne] < 64)
                    {
                        terrain[indiceLigne, indiceColonne].Type = "Foret";
                    }
                    if (tabTrame[indiceLigne, indiceColonne] >= 64)
                    {
                        terrain[indiceLigne, indiceColonne].Type = "Mer";
                    }
                }
            }
            return terrain;
        }

        /**
         * Fonction qui regroupe les cases en parcelles en fonction de leurs frontières
         * =============================================================================
         * :paramètre d'entrée Terrain: Case[,] : Terrain dont le type et les frontières ont déjà été attribués
         * :return Terrain: Case[,] : tableau de 10*10 cases contenant les informations suivantes :
         * Groupe contenant un caractère alphabétique en minuscule. 'a' pour la première parcelle, 'b' pour la seconde, etc.
         * Type défini correctement. Contient "Terre", "Mer" ou "Foret"
         * Booléens d'idenfications de frontière correctement initialisés
         */
        public static Case[,] DetectionParcelles(Case[,] terrain)
        {
            char charCourant = 'a';
            // parcours du tableau Terrain à l'aide de deux boucles "for" imbriquées
            for (int indiceX = 0; indiceX < 10; indiceX++)
            {
                for (int indiceY = 0; indiceY < 10; indiceY++)
                {
                    // Si la case est une plantation à associer à une parcelle...
                    if (terrain[indiceX, indiceY].Type == "Terre" && terrain[indiceX, indiceY].Groupe == ' ')
                    {
                        // ...on créer un groupe pour elle puis on lance la fonction Fouille pour détecter les cases dans sa parcelle
                        terrain[indiceX, indiceY].Groupe = charCourant;
                        Fouille(indiceX, indiceY, terrain, charCourant);
                        // On incrémente le char courant pour identifier la prochaine parcelle
                        charCourant++;
                    }
                    // Si la case n'est pas une plantatiob on lui faire rejoindre le groupe associé à son type
                    if (terrain[indiceX, indiceY].Type == "Mer")
                    {
                        terrain[indiceX, indiceY].Groupe = 'M';
                    }
                    if (terrain[indiceX, indiceY].Type == "Foret")
                    {
                        terrain[indiceX, indiceY].Groupe = 'F';
                    }
                }
            }
            return terrain;
        }

        /**
         * Fonction utilisée par la fonction "detection_parcelles" afin d'attribuer aux cases d'une même parcelle le même caractère de groupe
         * =============================================================================
         * :paramètres d'entrée indice_x, indice_y, Terrain, char_courant : int, int, Case[,], char : coordonnées x et y de la case étudiée, terrain étudié, char utilisé pour identifier la parcelle
         * :return Terrain: Case[,] : tableau de 10*10 cases contenant une version non finale du Terrain retourné par "detection_parcelle"
         */
        public static void Fouille(int indiceX, int indiceY, Case[,] terrain, char charCourant)
        {
            /** 
             * Si il n'y a pas de frontière dans une direction, c'est que la case adjacente dans cette direction fait partie de la même parcelle que la case étudiée.
             * On vérifie également que la case adjacente n'a pas déjà rejoint le groupe afin de ne pas boucler à l'infini dans la parcelle
             * On effectue ces deux tests pour toutes les directions, lorsqu'ils sont tout deux vérifés, la case ciblée rejoint la parcelle
             */
            if (terrain[indiceX, indiceY].FrontiereN == false)
            {
                if (terrain[(indiceX - 1), indiceY].Groupe == ' ')
                {
                    terrain[(indiceX - 1), indiceY].Groupe = charCourant;
                    Fouille((indiceX - 1), indiceY, terrain, charCourant);
                }
            }
            if (terrain[indiceX, indiceY].FrontiereE == false)
            {
                if (terrain[indiceX, (indiceY + 1)].Groupe == ' ')
                {
                    terrain[indiceX, (indiceY + 1)].Groupe = charCourant;
                    Fouille(indiceX, (indiceY + 1), terrain, charCourant);
                }
            }
            if (terrain[indiceX, indiceY].FrontiereS == false)
            {
                if (terrain[(indiceX + 1), indiceY].Groupe == ' ')
                {
                    terrain[(indiceX + 1), indiceY].Groupe = charCourant;
                    Fouille((indiceX + 1), indiceY, terrain, charCourant);
                }
            }
            if (terrain[indiceX, indiceY].FrontiereO == false)
            {
                if (terrain[indiceX, (indiceY - 1)].Groupe == ' ')
                {
                    terrain[indiceX, (indiceY - 1)].Groupe = charCourant;
                    Fouille(indiceX, (indiceY - 1), terrain, charCourant);
                }
            }
        }

        /**
         * Fonction appelant toutes les fonctions ci-dessus afin de créer un terrain opérationnel à partir d'une trame
         * =============================================================================
         * :paramètre d'entrée trame: String : suite de nombres séparés à l'aide de ';' et '|' identifiant un terrain de jeu précis
         * :return Terrain: Case[,] : tableau de 10*10 cases contenant une version non finale du Terrain retourné par "detection_parcelle"
         */
        public static Case[,] Decodage(string trame)
        {
            int[,] tabParc = DecoupageTrame(trame);
            Case[,] tabCaseF = DetermineFrontieresCases(tabParc);
            Case[,] tabCaseT = DetermineTypeCases(tabCaseF, tabParc);
            Case[,] Terrain = DetectionParcelles(tabCaseT);
            return Terrain;
        }

    }
}
