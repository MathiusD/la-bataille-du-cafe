﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesBatailleCafe
{
    public class Comptage_Points
    {
        /* Fonction permettant de déterminer les "points de paquet" de chaque joueur
         * Auteur : Paul ROSSELLE
         */
        public static int[] Compteur_points_paquet(Plateau terrain)
        {
            // Initialisation d'un tableau de booléens initialisé à False
            bool[,] tab_lu = new bool[10, 10];
            for (int ind_col = 0; ind_col < 10; ind_col++)
            {
                for (int ind_lig = 0; ind_lig < 10; ind_lig++)
                {
                    tab_lu[ind_col, ind_lig] = false;
                }
            }
            // Recherche du plus grand paquet de graines pour le J1 & le J2
            int PlusGrandPaquetJ1 = 0;
            int PlusGrandPaquetJ2 = 0;
            // Double boucle for permettant de consulter chaque case du terrain
            for (int ind_col = 0; ind_col < 10; ind_col++)
            {
                for (int ind_lig = 0; ind_lig < 10; ind_lig++)
                {
                    // On vérifie si la case n'a pas déjà été testée et si il y a une graine
                    if (tab_lu[ind_col, ind_lig] == false && terrain.Carte[ind_col, ind_lig].Graine != null)
                    {
                        // On détermine à quel joueur appartient la graine ciblée
                        if (terrain.Carte[ind_col, ind_lig].Graine.AQui.Nom == terrain.Participants[0].Nom)
                        {
                            /* On fouille autour de la case ciblée afin de voir si elle appartient à un paquet de graines
                             * La fouille est effectuée via une fonction récursive : "Recherche_graines_adjacentes"
                             */
                            int PlusGrandPaquetIter = Recherche_graines_adjacentes(0, terrain, tab_lu, 0, ind_col, ind_lig);
                            if (PlusGrandPaquetIter > PlusGrandPaquetJ1) {
                            PlusGrandPaquetJ1 = PlusGrandPaquetIter;
                            }
                        }
                        else if (terrain.Carte[ind_col, ind_lig].Graine.AQui.Nom == terrain.Participants[1].Nom)
                        {
                            int PlusGrandPaquetIter = Recherche_graines_adjacentes(1, terrain, tab_lu, 0, ind_col, ind_lig);
                            if (PlusGrandPaquetIter > PlusGrandPaquetJ2) {
                            PlusGrandPaquetJ2 = PlusGrandPaquetIter;
                            }
                        }
                    }

                }
            }
            int[] Score = { PlusGrandPaquetJ1, PlusGrandPaquetJ2 };
            return Score;
        }

        /* Fonction permettant de détecter toutes les graines appartenant au même joueur autour d'une graine cible
         * La fouille est faite en profondeur. À chaque fois qu'une graine adjacente à la graine ciblée appartenant 
         * au même joueur est détectée la fonction est appelée et à chaque retour de fonction, la valeur du compteur est 
         * remontée au "niveau supérieur" jusqu'à que plus aucune case ne soit détectée
         */
        public static int Recherche_graines_adjacentes(int num_Joueur, Plateau terrain, bool[,] tab_lu, int compteur, int ind_col, int ind_lig)
        {
            compteur += 1;
            tab_lu[ind_col, ind_lig] = true;
            if (ind_col != 9 && terrain.Carte[ind_col + 1, ind_lig].Graine != null)
            {
                if (terrain.Carte[ind_col + 1, ind_lig].Graine.AQui.Nom == terrain.Participants[num_Joueur].Nom && tab_lu[ind_col + 1, ind_lig] == false)
                {
                    compteur = Recherche_graines_adjacentes(num_Joueur, terrain, tab_lu, compteur, ind_col + 1, ind_lig);
                }
            }
            if (ind_col != 0 && terrain.Carte[ind_col - 1, ind_lig].Graine != null)
            {

                if (terrain.Carte[ind_col - 1, ind_lig].Graine.AQui.Nom == terrain.Participants[num_Joueur].Nom && tab_lu[ind_col - 1, ind_lig] == false)
                {
                    compteur = Recherche_graines_adjacentes(num_Joueur, terrain, tab_lu, compteur, ind_col - 1, ind_lig);
                }
            }
            if (ind_lig != 9 && terrain.Carte[ind_col, ind_lig + 1].Graine != null)
            {

                if (terrain.Carte[ind_col, ind_lig + 1].Graine.AQui.Nom == terrain.Participants[num_Joueur].Nom && tab_lu[ind_col, ind_lig + 1] == false)
                {
                    compteur = Recherche_graines_adjacentes(num_Joueur, terrain, tab_lu, compteur, ind_col, ind_lig + 1);
                }
            }
            if (ind_lig != 0 && terrain.Carte[ind_col, ind_lig - 1].Graine != null)
            {

                if (terrain.Carte[ind_col, ind_lig - 1].Graine.AQui.Nom == terrain.Participants[num_Joueur].Nom && tab_lu[ind_col, ind_lig - 1] == false)
                {
                    compteur = Recherche_graines_adjacentes(num_Joueur, terrain, tab_lu, compteur, ind_col, ind_lig - 1);
                }
            }
            return compteur;
        }

        /* Fonction permettant de déterminer les "points de parcelle" de chaque joueur
         * Auteur : Clément VIENNE & Paul ROSSELLE
         */
        public static int[] Compteur_points_parcelle(Plateau terrain)
        {
            /* Nous manipulons ici une liste de char et un tableau bidimensionnel afin de déterminer à qui "appartient" chaque parcelle
             * Nous stockons dans la liste de char le nom de chaque groupe constaté. 
             * À chaque char est associé (à l'indice [x][0] du tableau d'int) une "valeur d'appartenance"
             *  Si la valeur est positive à la fin de l'exploration, la parcelle appartient au J1, 
             *  si elle est négative elle appartient au J2. Si elle est nulle, elle n'appartient à aucun joueur
             * À chaque char est également associé (à l'indice [x][1] du tableau d'int) le nombre de cases de la parcelle
             */
            List<char> ListeGrp = new List<char>() { 'a' };
            List<int[]> TabVals = new List<int[]>();
            TabVals.Add(new int[] { 0, 0 });
            // Double boucle for permettant de consulter chaque case du terrain
            for (int ind_col = 0; ind_col < 10; ind_col++)
            {
                for (int ind_lig = 0; ind_lig < 10; ind_lig++)
                {
                    int ReturnIndice = verif_case_tab(terrain.Carte[ind_col, ind_lig].Groupe, ListeGrp, TabVals);
                    if (terrain.Carte[ind_col, ind_lig].Graine != null) {

                        if (terrain.Participants[0].Nom == terrain.Carte[ind_col, ind_lig].Graine.AQui.Nom) {
                            TabVals[ReturnIndice][0] += 1;
                        }
                        else
                        {
                            TabVals[ReturnIndice][0] -= 1;
                        }
                    }
                }
            }
            int[] TabScore = { 0, 0 };
            int IndiceEnCours = 0;
            foreach (char grp in ListeGrp) {
                    if (TabVals[IndiceEnCours][0] > 0) {
		            TabScore[0] += TabVals[IndiceEnCours][1];
	                }
                    else if (TabVals[IndiceEnCours][0] < 0)
                    {
		                TabScore[1] += TabVals[IndiceEnCours][1];
	                }
                    IndiceEnCours++;
            }
            return TabScore;
        }
    
    public static int verif_case_tab(char GroupeCase, List<char> ListeGrp, List<int[]> TabVals)
    {
        int IndiceTrouve = -1;
        int IndiceEnCours;

        for (IndiceEnCours = 0; IndiceEnCours < ListeGrp.Count; IndiceEnCours++)
            {
                if (ListeGrp[IndiceEnCours].Equals(GroupeCase))
                {
                    TabVals[IndiceEnCours][1] += 1;
                    IndiceTrouve = IndiceEnCours;
                }
            }

        if (IndiceTrouve == -1)
        {
            ListeGrp.Add(GroupeCase);
            TabVals.Add(new int[] { 0, 1 });
            IndiceTrouve = IndiceEnCours;
        }

        return IndiceTrouve;
    }

    public static int[] Compteur_points(Plateau terrain)
        {
            int[] points_paquets = Compteur_points_paquet(terrain);
            int[] points_parcelles = Compteur_points_parcelle(terrain);
            int[] Score = { points_paquets[0] + points_parcelles[0], points_paquets[1] + points_parcelles[1] };
            return Score;
        }
    }

}