﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesBatailleCafe
{
    public class Joueur
    {
        private string nom;
        private short nbGraines;
        private short nbPoints;

        public Joueur(string nom, short nbGraines, short nbPoints)
        {
            this.nom = nom;
            this.nbGraines = NbGraines;
            this.nbPoints = NbPoints;
        }
        public Joueur(Joueur j) : this(j.Nom, j.NbGraines, j.NbPoints)
        {

        }
        public Joueur(string nom) : this(nom, 28, 0)
        {
            
        }

        public short NbPoints { get => nbPoints; set => nbPoints = value; }

        public short NbGraines { get => nbGraines; set => nbGraines = value; }

        public string Nom { get => nom; }
    }
}
