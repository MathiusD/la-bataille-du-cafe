﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace ClassesBatailleCafe
{
    public class IA_Algo
    {

        /**
         * Fonction de choix de pose
         * -------------------------
         * Cette fonction choisie quel coup jouer en fonction de la meilleur 
         *  possibilité
         *  
         * in : Plateau p_plateau : plateau de jeu
         * in : byte p_profondeurActuelle : profondeur actuelle de l'algo
         * in : byte p__profondeurMax : profondeur max de l'algo
         * in : byte p_indexJoueur : index du joueur dont c'est le tour
         * in : Socket p_serv : pour communiquer avec le serveur
         */

        public static void ChoixPose(Plateau p_plateau, byte p_profonfeurActuelle,
            byte p_profondeurMax, byte p_indexJoueur, Socket p_serv)

        {
            // Pour stocker la meilleure graine et la différence de point
            byte[] v_meilleureGraine;
            if (p_plateau.DerniereGrainePosee != null)
                v_meilleureGraine = VerificationPourLaPose(p_plateau, p_profondeurMax, p_profonfeurActuelle, p_indexJoueur);
            else
            {
                v_meilleureGraine = DefaultChoice(p_plateau);
            }
            p_plateau.addGraine(v_meilleureGraine[0], v_meilleureGraine[1], 0);
            Communication_Serveur.SendToServ(p_serv, String.Format("A:{0}{1}", v_meilleureGraine[0], v_meilleureGraine[1]));
            // Pose de la meilleure graine sur le plateau et envoi du coup au serveur
            
        }

        private static byte[] DefaultChoice(Plateau p_plateau)
        {
            byte[] v_default_choice = {0, 0};
            bool Verif = true;
            for (byte cpt = 0; cpt < 10 && Verif; cpt++)
            {
                for(byte cpt2 = 0; cpt2 < 10 && Verif; cpt2++)
                {
                    if (Utils.VerifPose(p_plateau, cpt, cpt2))
                    {
                        v_default_choice[0] = cpt;
                        v_default_choice[1] = cpt2;
                        Verif = false;
                    }
                }
            }
            return v_default_choice;
        }

        private static byte[] VerificationPourLaPose(Plateau p_plateau, byte p_profondeurMax, byte p_profonfeurActuelle, byte p_indexJoueur)
        {
            if (p_profondeurMax > 0)
            {
                byte CoordX = (byte)p_plateau.DerniereGrainePosee[0];
                byte CoordY = (byte)p_plateau.DerniereGrainePosee[1];
                byte[] v_meilleureGraine = { 0, 0 };
                int v_nbrVictoireMax = 0;

                for(byte lig = 0; lig < 10; lig++)
                {
                    if(Utils.VerifPose(p_plateau, lig, CoordY))
                    {
                        Plateau v_autreplateau = new Plateau(p_plateau);
                        v_autreplateau.addGraine(lig, CoordY, p_indexJoueur);

                        byte v_joueurSuivant = 0;
                        if (p_indexJoueur == 0)
                            v_joueurSuivant = 1;

                        int v_tmp = VerifCol(v_autreplateau, v_joueurSuivant, p_profondeurMax, p_profonfeurActuelle++);
                        if(v_nbrVictoireMax < v_tmp)
                        {
                            v_nbrVictoireMax = v_tmp;
                            v_meilleureGraine[0] = lig;
                            v_meilleureGraine[1] = CoordY;
                        }
                    }
                }
                for(byte col = 0; col < 10; col++)
                {
                    if(Utils.VerifPose(p_plateau, CoordX, col))
                    {
                        Plateau v_autreplateau = new Plateau(p_plateau);
                        v_autreplateau.addGraine(CoordX, col, p_indexJoueur);

                        byte v_joueurSuivant = 0;
                        if (p_indexJoueur == 0)
                            v_joueurSuivant = 1;

                        int v_tmp = VerifLigne(v_autreplateau, v_joueurSuivant, p_profondeurMax, p_profonfeurActuelle++);
                        if(v_nbrVictoireMax < v_tmp)
                        {
                            v_nbrVictoireMax = v_tmp;
                            v_meilleureGraine[0] = CoordX;
                            v_meilleureGraine[1] = col;
                        }
                    }
                }
                if (v_nbrVictoireMax == 0)
                    v_meilleureGraine = VerificationPourLaPose(p_plateau, (byte)(p_profondeurMax- 1), p_profonfeurActuelle, p_indexJoueur);
                return v_meilleureGraine;
            }
            else
                return DefaultChoice(p_plateau);
        }

        private static int VerifLigne(Plateau p_plateau, byte p_indexJoueur, byte p_profondeurMax, byte p_profondeurActu)
        {
            short[] DerGraine = p_plateau.DerniereGrainePosee;
            byte CoordX = (byte)DerGraine[0];
            int nbrVictoire = 0;
            for (byte col = 0; col < 10; col++)
            {
                if (Utils.VerifPose(p_plateau, CoordX, col))
                {
                    Plateau v_autrePlateau = new Plateau(p_plateau);
                    v_autrePlateau.addGraine(CoordX, col, p_indexJoueur);
                    byte v_joueurSuivant = 0;
                    if (p_indexJoueur == 0)
                        v_joueurSuivant = 1;
                    nbrVictoire += RecursionCall(v_autrePlateau, v_joueurSuivant, p_profondeurMax, p_profondeurActu++);
                }
            }
            return nbrVictoire;
        }


        private static int VerifCol(Plateau p_plateau, byte p_indexJoueur, byte p_profondeurMax, byte p_profondeurActu)
        {

            short[] DerGraine = p_plateau.DerniereGrainePosee;
            byte CoordY = (byte)DerGraine[1];
            int nbrVictoire = 0;

            for (byte ligne = 0; ligne < 10; ligne++)
            {
                if (Utils.VerifPose(p_plateau, ligne, CoordY))
                {
                    Plateau v_autrePlateau = new Plateau(p_plateau);
                    v_autrePlateau.addGraine(ligne, CoordY, p_indexJoueur);
                    byte v_joueurSuivant = 0;
                    if (p_indexJoueur == 0)
                        v_joueurSuivant = 1;
                    nbrVictoire += RecursionCall(v_autrePlateau, v_joueurSuivant, p_profondeurMax, p_profondeurActu++);
                }
            }
            return nbrVictoire;
        }

        private static int RecursionCall(Plateau p_plateau, byte p_joueurSuivant, byte p_profondeurMax, byte p_profondeurActu)
        {
            if (p_profondeurActu < p_profondeurMax)
            {
                int v_tmpCol = VerifCol(p_plateau, p_joueurSuivant, p_profondeurMax,p_profondeurActu++);
                int v_tmpLigne = VerifLigne(p_plateau, p_joueurSuivant, p_profondeurMax,p_profondeurActu++);
                return v_tmpCol + v_tmpLigne;
            }
            else
            {
                int[] tmp = Utils.CompteurPoints(p_plateau);
                if(tmp[0] > tmp[1])
                    return 1;
                else
                    return 0;
            }
        }
    }
}