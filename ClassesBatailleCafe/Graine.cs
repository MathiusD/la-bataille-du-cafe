﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesBatailleCafe
{
    public class Graine
    {
        private Joueur aQui;

        public Graine(Joueur aQui)
        {
            this.aQui = aQui;
        }

        internal Joueur AQui { get => aQui; }

    }
}
