﻿using System;
using System.Text;
using System.Net.Sockets;

namespace ClassesBatailleCafe
{
    public class Communication_Serveur
    {
        /*
        Définition de la méthode Connect_Serv
        Cette dernière permet juste de simplifier la connection
        à l'aide de Sockets.
        Elle demande en paramètre l'host et le port où l'on
        souhaite se connecter. La connection est alors établie
        en TCP.
        */
        public static Socket ConnectServ(string host, int port)
        {
            Socket sServ = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sServ.Connect(host, port);
            return sServ;
        }
        /*
        Définition de la méthode Send_To_Serv
        Cette dernière permet juste de simplifier l'envoi
        de données à travers des Sockets.
        Elle demande en paramètre la Sockets et le message à
        envoyer sous forme de String. On converti la chaine
        en tableau de byte avant de l'envoyer au serveur.
        */
        public static void SendToServ(Socket sServ, String dataOut)
        {
            byte[] dataServ = Encoding.ASCII.GetBytes(dataOut);
            sServ.Send(dataServ);
        }
        /*
        Définition de la méthode Receive_To_Serv
        Cette dernière permet juste de simplifier la reception
        de données à travers des Sockets.
        Elle demande en paramètre la Sockets et la taille du
        message attendu. Une fois le message reçu on le décode
        et le place dans une String que l'on renvoie.
        */
        public static String ReceiveToServ(Socket sServ, int length)
        {
            byte[] dataServ = new byte[length];
            sServ.Receive(dataServ);
            String dataIn = Encoding.ASCII.GetString(dataServ);
            return dataIn;
        }
        /*
        Définition de la méthode Close_Serv
        Cette dernière permet juste de simplifier la fermeture
        de Sockets.
        Elle demande en paramètre la Socket et ferme la connection
        'proprement' en attendant que toutes les données soient
        transmise avant d'effectivement clore la connection.
        */
        public static void CloseServ(Socket sServ)
        {
            sServ.Shutdown(SocketShutdown.Both);
            sServ.Close();
        }
    }
}
