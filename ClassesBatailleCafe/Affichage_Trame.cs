﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassesBatailleCafe;

namespace ClassesBatailleCafe
{
    public class Affichage_Trame
    {
        /*
         * Affichage_Trame : fonction qui permet d'afficher dans la console les cases disponibles
         * ou non
         * Paramètre : cette fonction prends un plateau
         * Sortie : Rien
         * Affichage : Affiche la carte en couleur
         */
        public static void AffichageTrame(Plateau plateau, Joueur J1, Joueur J2)
        {
            Case[,] carteCase = plateau.Carte;
            Console.ForegroundColor = ConsoleColor.Black;

            for (short compteur = 0; compteur <10; compteur ++)
            {
                for (short compteur2 = 0; compteur2 <10; compteur2 ++)
                {
                    //On récupère la case que l'on va afficher
                    Case caseAAfficher = carteCase[compteur,compteur2];
                    short[] coordonnee = { compteur, compteur2 };

                    //Si cette case a une graine, on entre dans cette boucle
                    if(caseAAfficher.Graine!=null)
                    {
                        short[] coordDerGrai = { -1, -1 };
                        if (plateau.DerniereGrainePosee != null)
                        {
                            coordDerGrai[0] = plateau.DerniereGrainePosee[0];
                            coordDerGrai[1] = plateau.DerniereGrainePosee[1];
                        }
                        if (coordonnee[0] == coordDerGrai[0] && coordonnee[1] == coordDerGrai[1])
                        {
                            /*
                                * Si cette case contient la dernière graine posée
                                * On affiche la case avec un fond Jaune, pour la différencier des autres
                                */
                            Console.BackgroundColor = ConsoleColor.Yellow;
                            Console.Write(" {0} ", carteCase[compteur, compteur2].Groupe);
                            Console.BackgroundColor = ConsoleColor.Black;
                        }
                        
                        else
                        {
                            if (caseAAfficher.Graine.AQui.Nom == J1.Nom)
                            {
                                Console.BackgroundColor = ConsoleColor.Red;
                            }
                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Magenta;
                            }
                            Console.Write(" {0} ", carteCase[compteur, compteur2].Groupe);
                            Console.BackgroundColor = ConsoleColor.Black;
                        }
                        
                    }
                    else
                    {

                        /*
                         * Si cette case n'a pas de graine MAIS qu'elle est de Type Terre
                         * On l'affiche avec un fond Gris, pour signifier qu'elle est disponible
                         */
                        if(carteCase[compteur,compteur2].Type == "Terre")
                        {
                            Console.BackgroundColor = ConsoleColor.Gray;
                            Console.Write(" {0} ", carteCase[compteur, compteur2].Groupe);
                            Console.BackgroundColor = ConsoleColor.Black;
                        }
                        else
                        {

                            /*
                             * Si cette case est de Type Mer
                             * On affiche la case avec un fond Bleu, qui permet de dire que
                             * cette case est une case Mer
                             */
                            if(carteCase[compteur,compteur2].Type == "Mer")
                            {
                                Console.BackgroundColor = ConsoleColor.Blue;
                                Console.Write("   ");
                                Console.BackgroundColor = ConsoleColor.Black;
                            }

                            /*
                             * Si cette case est de type Forêt
                             * On l'affiche dans un fond Vert, ce qui nous prévient que cette
                             * case est de type forêt
                             */
                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Green;
                                Console.Write("   ");
                                Console.BackgroundColor = ConsoleColor.Black;
                               
                            }
                        }
                    }
                
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            AffichageExplique();
            Console.ForegroundColor = ConsoleColor.White;
        }

        /*
         * Cette fonction permet de noter dans le terminal à quoi correspondent les couleurs utilisées
         * pour l'affichage
         * exemple : Bleu = Mer
         */
        public static void AffichageExplique()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine("Case Mer");
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine("Case Forêt");
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.WriteLine("Case Terre libre");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("Case Terre prise par le Joueur 1");
            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Case Terre prise par le joueur 2");
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Case Terre avec la dernière graine posée");
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
