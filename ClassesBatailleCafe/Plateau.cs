﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesBatailleCafe
{
    public class Plateau
    {
        private Case[,] carte;
        private short[] derniereGrainePosee;
        private Joueur[] participants;

        public Plateau(Case[,] carte, Joueur[] participants)
        {
            this.participants = participants;
            this.carte = carte;
        }

        public Plateau(Plateau plateau)
        {
            this.carte = new Case[10,10];
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    this.carte[x,y] = new Case(plateau.carte[x,y]);
                }
            }
            this.participants = new Joueur[2];
            for (int y = 0; y < 2; y++)
            {
                this.participants[y] = new Joueur(plateau.participants[y]);
            }
            if (plateau.derniereGrainePosee != null)
            {
                this.derniereGrainePosee = new short[2];
                for (int y = 0; y < 2; y++)
                {
                    short shorttmp = plateau.derniereGrainePosee[y];
                    this.derniereGrainePosee[y] = shorttmp ;
                }
            }
        }

        public Case[,] Carte { get => carte; }

        public Joueur[] Participants { get => participants; }
        public short[] DerniereGrainePosee { get => derniereGrainePosee; set => derniereGrainePosee = value; }

        public void addGraine(byte x, byte y, byte indice_joueur)
        {
            Graine graine = new Graine(this.participants[indice_joueur]);
            this.carte[x, y].Graine = graine;
            short[] coordonnee = { x, y };
            DerniereGrainePosee = coordonnee ;
            this.participants[indice_joueur].NbGraines -= 1;
        }

    }
}

 

