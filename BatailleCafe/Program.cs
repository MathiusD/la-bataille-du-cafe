﻿using System;
using System.Net.Sockets;
using ClassesBatailleCafe;

namespace BatailleCafe
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket serv = Communication_Serveur.ConnectServ("localhost", 1213);
            String trame = Communication_Serveur.ReceiveToServ(serv, 300);
            Joueur J1 = new Joueur("Blap");
            Joueur J2 = new Joueur("Blip");
            Joueur[] J = new Joueur[2];
            J[0] = J1;
            J[1] = J2;
            Plateau plateau = new Plateau(Decodage_Trame.Decodage(trame), J);
            Affichage_Trame.AffichageTrame(plateau, J1, J2);
            String msg = "";
            do
            {
                IA_Algo.ChoixPose(plateau, 0, 4, 0, serv);
                Affichage_Trame.AffichageTrame(plateau, J1, J2);
                // 1er message
                msg = Communication_Serveur.ReceiveToServ(serv, 4);

                // 2ème message
                msg = Communication_Serveur.ReceiveToServ(serv, 4);
                String xS = String.Format("{0}", msg[2]);
                String yS = String.Format("{0}", msg[3]);
                if (!msg.Equals("FINI"))
                {
                    plateau.addGraine(Byte.Parse(xS), Byte.Parse(yS), 1);
                    Affichage_Trame.AffichageTrame(plateau, J1, J2);

                    // 3eme message
                    msg = Communication_Serveur.ReceiveToServ(serv, 4);
                }
            } while  (!msg.Equals("FINI"));
            Console.WriteLine(Communication_Serveur.ReceiveToServ(serv, 7));
            Communication_Serveur.CloseServ(serv);
        }
    }
}
