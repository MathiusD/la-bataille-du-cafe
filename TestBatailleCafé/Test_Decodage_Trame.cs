﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassesBatailleCafe;

namespace TestBatailleCafé
{
    [TestClass]
    public class Test_Decodage_Trame
    {
        /**
         * Fonction de création d'un tableau d'entier de parcelles
         * =======================================================
         * :return tab_parc: int[,] : tableau de parcelles
         */
        public int[,] CreationTabParc()
        {
            int[,] tabParc = new int[10,10];
            tabParc[0,0] = 3;
            tabParc[0,1] = 9;
            tabParc[0,2] = 71;
            tabParc[0,3] = 69;
            tabParc[0,4] = 65;
            tabParc[0,5] = 65;
            tabParc[0,6] = 65;
            tabParc[0,7] = 65;
            tabParc[0,8] = 65;
            tabParc[0,9] = 73;

            tabParc[1,0] = 2;
            tabParc[1,1] = 8;
            tabParc[1,2] = 3;
            tabParc[1,3] = 9;
            tabParc[1,4] = 70;
            tabParc[1,5] = 68;
            tabParc[1,6] = 64;
            tabParc[1,7] = 64;
            tabParc[1,8] = 64;
            tabParc[1,9] = 72;

            tabParc[2,0] = 6;
            tabParc[2,1] = 12;
            tabParc[2,2] = 2;
            tabParc[2,3] = 8;
            tabParc[2,4] = 3;
            tabParc[2,5] = 9;
            tabParc[2,6] = 70;
            tabParc[2,7] = 68;
            tabParc[2,8] = 64;
            tabParc[2,9] = 72;

            tabParc[3,0] = 11;
            tabParc[3,1] = 11;
            tabParc[3,2] = 6;
            tabParc[3,3] = 12;
            tabParc[3,4] = 6;
            tabParc[3,5] = 12;
            tabParc[3,6] = 3;
            tabParc[3,7] = 9;
            tabParc[3,8] = 70;
            tabParc[3,9] = 76;

            tabParc[4,0] = 10;
            tabParc[4,1] = 10;
            tabParc[4,2] = 11;
            tabParc[4,3] = 11;
            tabParc[4,4] = 67;
            tabParc[4,5] = 73;
            tabParc[4,6] = 6;
            tabParc[4,7] = 12;
            tabParc[4,8] = 3;
            tabParc[4,9] = 9;

            tabParc[5,0] = 14;
            tabParc[5,1] = 14;
            tabParc[5,2] = 10;
            tabParc[5,3] = 10;
            tabParc[5,4] = 70;
            tabParc[5,5] = 76;
            tabParc[5,6] = 7;
            tabParc[5,7] = 13;
            tabParc[5,8] = 6;
            tabParc[5,9] = 12;

            tabParc[6,0] = 3;
            tabParc[6,1] = 9;
            tabParc[6,2] = 14;
            tabParc[6,3] = 14;
            tabParc[6,4] = 11;
            tabParc[6,5] = 7;
            tabParc[6,6] = 13;
            tabParc[6,7] = 3;
            tabParc[6,8] = 9;
            tabParc[6,9] = 75;

            tabParc[7,0] = 2;
            tabParc[7,1] = 8;
            tabParc[7,2] = 7;
            tabParc[7,3] = 13;
            tabParc[7,4] = 14;
            tabParc[7,5] = 3;
            tabParc[7,6] = 9;
            tabParc[7,7] = 6;
            tabParc[7,8] = 12;
            tabParc[7,9] = 78;

            tabParc[8,0] = 6;
            tabParc[8,1] = 12;
            tabParc[8,2] = 3;
            tabParc[8,3] = 1;
            tabParc[8,4] = 9;
            tabParc[8,5] = 6;
            tabParc[8,6] = 12;
            tabParc[8,7] = 35;
            tabParc[8,8] = 33;
            tabParc[8,9] = 41;

            tabParc[9,0] = 71;
            tabParc[9,1] = 77;
            tabParc[9,2] = 6;
            tabParc[9,3] = 4;
            tabParc[9,4] = 12;
            tabParc[9,5] = 39;
            tabParc[9,6] = 37;
            tabParc[9,7] = 36;
            tabParc[9,8] = 36;
            tabParc[9,9] = 44;

            return tabParc;
        }

        [TestMethod]
        /**
         * Test de découpageTrame
         */
        public void TestDecoupageTrame()
        {
            string trame = "3:9:71:69:65:65:65:65:65:73|" +
                "2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|" +
                    "11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|" +
                        "14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|" +
                            "2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|" +
                                "71:77:6:4:12:39:37:36:36:44|";
            int[,] tabParcVerif = CreationTabParc();
            int[,] tabResul = Decodage_Trame.DecoupageTrame(trame);

            for(int compt1=0; compt1<10; compt1++)
                for(int compt2=0;compt2<10;compt2++)
                    Assert.AreEqual(tabParcVerif[compt1,compt2], tabResul[compt1,compt2]);
        }

        [TestMethod]
        /**
         * Test de détermineFrontièresCases
         */
        public void TestDetermineFrontieresCases()
        {
            int[,] tabParc = CreationTabParc();
            Case[,] tabCase = Decodage_Trame.DetermineFrontieresCases(tabParc);
            Assert.IsTrue(tabCase[0, 0].FrontiereN);
            Assert.IsTrue(tabCase[0, 0].FrontiereO);
            Assert.IsFalse(tabCase[0, 0].FrontiereS);
            Assert.IsFalse(tabCase[0, 0].FrontiereE);
        }

        [TestMethod]
        /**
         * Test de détermineTypeParcelles
         */
        public void TestDetermineTypeCases()
        {
            int[,] tabParc = CreationTabParc();
            Assert.AreEqual(Decodage_Trame.DetermineTypeCases(Decodage_Trame.DetermineFrontieresCases(tabParc), tabParc)[0,0].Type, "Terre");
            Assert.AreEqual(Decodage_Trame.DetermineTypeCases(Decodage_Trame.DetermineFrontieresCases(tabParc), tabParc)[0,2].Type, "Mer");
            Assert.AreEqual(Decodage_Trame.DetermineTypeCases(Decodage_Trame.DetermineFrontieresCases(tabParc), tabParc)[9,8].Type, "Foret");
        }

        [TestMethod]
        /**
         * Test du regroupement des cases en parcelles
         */
        public void TestDetectionParcelles()
        {
            string trame = "3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|" +
                "6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|" +
                "14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78" +
                "|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44|";
            int[,] tabParc = Decodage_Trame.DecoupageTrame(trame);
            Case[,] tabCaseF = Decodage_Trame.DetermineFrontieresCases(tabParc);
            Case[,] tabCaseT = Decodage_Trame.DetermineTypeCases(tabCaseF, tabParc);
            Case[,] tabCase = Decodage_Trame.DetectionParcelles(tabCaseT);
            Assert.AreEqual(tabCase[1, 1].Groupe, 'a');
            Assert.AreEqual(tabCase[4, 7].Groupe, 'f');
            Assert.AreEqual(tabCase[5, 0].Groupe, 'd');
            Assert.AreEqual(tabCase[9, 9].Groupe, 'F');
        }

        [TestMethod]
        /**
         * Test décodage en entier en réutilisant les autre fonctions
         */
        public void TestDecodage()
        {
            string trame = "3:9:71:69:65:65:65:65:65:73|" +
                "2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|" +
                    "11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|" +
                        "14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|" +
                            "2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|" +
                                "71:77:6:4:12:39:37:36:36:44|";
            Case[,] tabCase = Decodage_Trame.Decodage(trame);
            Assert.AreEqual(tabCase[0, 0].Type, "Terre");
            Assert.AreEqual(tabCase[0, 2].Type, "Mer");
            Assert.AreEqual(tabCase[9, 8].Type, "Foret");

            Assert.IsTrue(tabCase[0, 0].FrontiereN);
            Assert.IsTrue(tabCase[0, 0].FrontiereO);
            Assert.IsFalse(tabCase[0, 0].FrontiereS);
            Assert.IsFalse(tabCase[0, 0].FrontiereE);

            Assert.AreEqual(tabCase[1, 1].Groupe, 'a');
            Assert.AreEqual(tabCase[4, 7].Groupe, 'f');
            Assert.AreEqual(tabCase[5, 0].Groupe, 'd');
            Assert.AreEqual(tabCase[9, 9].Groupe, 'F');
        }
    }
    }
