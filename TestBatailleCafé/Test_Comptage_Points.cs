﻿using System;
using ClassesBatailleCafe;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestBatailleCafé
{
    [TestClass]
    public class Test_Comptage_Points
    {
        public Plateau CreationPlateau()
        {
            // Création du tableau de case
            string trame = "3:9:71:69:65:65:65:65:65:73|" +
                 "2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|" +
                     "11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|" +
                         "14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|" +
                             "2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|" +
                                 "71:77:6:4:12:39:37:36:36:44|";
            Case[,] tabCase = Decodage_Trame.Decodage(trame);

            // Création du tableau de joueurs
            Joueur J1 = new Joueur("J1");
            Joueur J2 = new Joueur("J2");
            Joueur[] Joueurs = { J1, J2 };

            // Placement des graines dans le tableau de cases
            tabCase[2, 1].Graine = new Graine(J1);
            tabCase[3, 0].Graine = new Graine(J1);
            tabCase[3, 6].Graine = new Graine(J1);
            tabCase[4, 0].Graine = new Graine(J1);
            tabCase[4, 6].Graine = new Graine(J1);
            tabCase[5, 0].Graine = new Graine(J1);
            tabCase[5, 6].Graine = new Graine(J1);
            tabCase[6, 5].Graine = new Graine(J1);
            tabCase[8, 2].Graine = new Graine(J1);

            tabCase[1, 1].Graine = new Graine(J2);
            tabCase[1, 2].Graine = new Graine(J2);
            tabCase[1, 3].Graine = new Graine(J2);
            tabCase[2, 3].Graine = new Graine(J2);
            tabCase[3, 3].Graine = new Graine(J2);
            tabCase[8, 3].Graine = new Graine(J2);
            tabCase[9, 3].Graine = new Graine(J2);

            // Création du plateau
            Plateau Terrain = new Plateau(tabCase, Joueurs);

            return Terrain;
        }

        [TestMethod]
        /**
         * Test de Compteur_points_paquet
         */
        public void TestCompteur_points_paquet()
        {
            // Création du plateau
            Plateau Terrain = CreationPlateau();
            // Création du tableau contenant les valeurs à comparer
            int[] tab_result = { 3, 5 };

            int[] tab_verif = Comptage_Points.Compteur_points_paquet(Terrain);
            Assert.AreEqual(tab_verif[0], tab_result[0]);
            Assert.AreEqual(tab_verif[1], tab_result[1]);
        }

        [TestMethod]
        /**
         * Test de Compteur_points_parcelle
         */
        public void  TestCompteur_points_parcelle()
        {
            // Création du plateau
            Plateau Terrain = CreationPlateau();
            // Création du tableau contenant les valeurs à comparer
            int[] tab_result = { 11, 12 };

            int[] tab_verif = Comptage_Points.Compteur_points_parcelle(Terrain);
            Assert.AreEqual(tab_verif[0], tab_result[0]);
            Assert.AreEqual(tab_verif[1], tab_result[1]);
        }

        [TestMethod]
        /**
         * Test de Compteur_points
         */
        public void TestCompteur_points()
        {
            // Création du plateau
            Plateau Terrain = CreationPlateau();
            // Création du tableau contenant les valeurs à comparer
            int[] tab_result = { 14, 17 };

            int[] tab_verif = Comptage_Points.Compteur_points(Terrain);
            Assert.AreEqual(tab_verif[0], tab_result[0]);
            Assert.AreEqual(tab_verif[1], tab_result[1]);
        }
    }
}
