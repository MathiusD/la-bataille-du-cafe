# La bataille du cafe

![pipeline](https://gitlab.com/MathiusD/la-bataille-du-cafe/badges/master/pipeline.svg)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ce813da0bf414c8c898926e6c0a6b737)](https://www.codacy.com/manual/MathiusD/la-bataille-du-cafe?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathiusD/la-bataille-du-cafe&amp;utm_campaign=Badge_Grade)

Projet tutoré la Bataille du Café par l'équipe K Fée

## About

Pour lancer le projet

```bash
cd BatailleCafe; dotnet run;
```

Pour les tests

```bash
dotnet tests
```

## Authors

* Mathieu FERY
* Clément VIENNE
* Mathilde BALLOUHEY
* Paul Rosselle
